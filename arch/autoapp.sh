#!/usr/bin/env bash



sudo pacman -S \
	alsa-plugins \
	awesome \
	baobab \
	bc \
	blueman \
	bluez \
	bluez-utils \
	celluloid \
	cinnamon \
	clamav \
	clipgrab \
	cmus \
	cups \
	cups-filters \
	cups-pdf \
	cups-pk-helper \
	eog \
	ffmpeg \
	firefox \
	firefox-ublock-origin \
	freerdp \
	ghostscript \
	gimp \
	gnome-calculator \
	gnome-disk-utility \
	gnome-screenshot \
	gnome-system-monitor \
	gst-libav \
	hplip \
	htop \
	hunspell-en_ca \
	imwheel \
	kdenlive \
	libdvdcss \
	libdvdnav \
	libdvdread \
	libqalculate \
	libreoffice-fresh \
	lightdm \
	lightdm-gtk-greeter \
	lightdm-gtk-greeter-settings \
	lxappearance-gtk3 \
	man-db \
	man-pages \
	materia-gtk-theme \
	nemo \
	neofetch \
	ntfs-3g \
	numlockx \
	obs-studio \
	papirus-icon-theme \
	pavucontrol \
	pcmanfm \
	physlock \
	print-manager \
	pulseaudio \
	pulseaudio-alsa \
	pulseaudio-bluetooth \
	qtile \
	ranger \
	remmina \
	rhythmbox \
	system-config-printer \
	terminus-font \
	ufw \
	xed \
	xfce4-power-manager \
	xorg-server \
	xorg-xinit \
	xorg-xrandr \
	xterm \
	xvidcore \
	zsh \
	zsh-syntax-highlighting
