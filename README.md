# Mench


## About Artibus/Mench

Welcome to Mench! These are the configuration files I use for various distros
and flavors of Linux.

If you would like to see these configurations in action, you can check out the
[Linux Mench YouTube Channel][1], where I post videos about my experiences with
various Linux distros.


## Project Structure

The structure of the files in this repository is easy to understand:

- `arch` contains configuration files for Arch Linux and derived distros
   - Check out the [Arch Linux playlist][4] on my channel to see me using these configs
- `debian` contains configuration files for Debian-based distros
   - I have a playlist called [Debian Based Distros][2] where you can see these files in action
- `freebsd` holds configs for BSD installations
   - You can check out my [BSD playlist][3] to see these configs in action
- `LinuxMint-Ubuntu` contains files specifically for Ubuntu-based versions of Linux Mint

Each directory contains a file called `autoapp.sh`: this is a script I use to
easily install all the packages I want on a given distro. You may have to install
`git` with your distro's package manager before you can clone this repository
and use these files.


## Usage

Installing these files is very simple:

1. Make sure `git` is installed on your system
2. Clone this repo (I usually clone it into `~/mench`)
3. (Optional) Run `./autoapp.sh` if you would like to install the packages I use
4. Copy the configuration files from the repo to your home folder
   - i.e. `cp .xinitrc .Xresources ~/`
5. You may need to log out and log back in for some changes to take effect.
6. You're done! Enjoy!


[1]: https://youtube.com/channel/UCGDU_C4_WkaLxBtmNDeBUXw
[2]: https://www.youtube.com/playlist?list=PL5jHfB9wTlwSt-mFsqnZuUc9MSBG_ZXZk
[3]: https://www.youtube.com/playlist?list=PL5jHfB9wTlwQO6Tdc4_7Qk5wOFeRFieeW
[4]: https://www.youtube.com/playlist?list=PL5jHfB9wTlwSjLNzAYGPb8JBcwPhBksZ6
